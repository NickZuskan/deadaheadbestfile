﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    public GameObject playableCharacter;
    public GameObject fovStartPoint;
    public float lookSpeed = 200;
    public float maxAngle = 90;

    private Quaternion targetRotation;
    private Quaternion lookAt;



    void Update()
    {
        if (PlayableCharacterInFOV(fovStartPoint))//player will be looking at target 
        {
            Vector3 direction = playableCharacter.transform.position - transform.position;
            targetRotation = Quaternion.LookRotation(direction);
            lookAt = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * lookSpeed);
            transform.rotation = lookAt;
        }
        else//player will look freely
        {
            targetRotation = Quaternion.Euler(0, 0, 0);
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetRotation, Time.deltaTime * lookSpeed);
        }


    }

    bool PlayableCharacterInFOV(GameObject looker)
    {
        Vector3 targetDir = playableCharacter.transform.position - transform.position;
        float angle = Vector3.Angle(targetDir, looker.transform.forward);
        
        if (angle < maxAngle)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
}
